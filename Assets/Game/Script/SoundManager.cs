﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum OtherSounds
{
    PlayerAtk,
    PlayerHurt,
    EnemyDefeated,
}
public class SoundManager : MonoBehaviour
{
    [Header("BasicAudio")]
    public AudioClip playingBGM;
    public AudioClip metronomeTic;
    public AudioClip metronomeToc;

    [Header("Others")]
    public AudioClip playerAtk;
    public AudioClip playerHurt;
    public AudioClip enemyDefeated;
    
    [SerializeField] private AudioSource _audioSourceBGM;
    [SerializeField] private AudioSource _audioSourceMetronome;
    [SerializeField] private AudioSource _audioSourceOther;



    public static SoundManager Instance;
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySoundBGM(AudioClip clip)
    {
        //_audioSourceBGM.clip = clip;
        _audioSourceBGM.PlayOneShot(clip);
    }
    
    
    public void PlaySoundMetro(AudioClip clip)
    {
        //_audioSourceMetronome.clip = clip;
        _audioSourceMetronome.PlayOneShot(clip);
    }
    
    public void PlaySoundOther(OtherSounds sound)
    {
        switch (sound)
        {
            case OtherSounds.EnemyDefeated :
                _audioSourceOther.PlayOneShot(enemyDefeated);
                break;
            case OtherSounds.PlayerAtk :
                _audioSourceOther.PlayOneShot(playerAtk);
                break;
            case OtherSounds.PlayerHurt : 
                _audioSourceOther.PlayOneShot(playerHurt);
                break;
        }
    }

    public void ModifyPitchBGM(float multiplier)
    {
        Debug.Log(multiplier);
        _audioSourceBGM.pitch = 1*multiplier;
    }
}

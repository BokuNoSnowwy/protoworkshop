﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class WaveEnemy
{
    public int bpmDifficulty;
    public int waveNumber;
    public int indexEnemy;
    public List<GameObject> enemyList = new List<GameObject>();
}
public class GameplayManager : MonoBehaviour
{
    [Header("LifePlayer")] 
    public Player player;
    public int lifePlayer;
    public TextMeshProUGUI labelLife;
    public Transform playerSpawn;

    [Header("Ryhthm")] 
    public int bpm;
    public float secondBeat;
    public bool playerCanMove;
    public float resetTimerEachBeat;
    public float timerEachBeat;
    [SerializeField] public float spdMultiplier;

    private int indexMetronome = 0;

    [Header("Juice")] 
    private bool juiceIsTriggered;
    public Image ryhthmPanel;
    public Image gamePanel;
    
    [Header("EnnemyWaves")]
    public TextMeshProUGUI labelWave;
    public Transform enemySpot;
    public Transform enemySpawn;
    
    //TODO Spawn enemy after a specific number of beats
    public int beatsToSpawn;
    public int indexBeatsSpawn;
    
    [SerializeField]
    public List<WaveEnemy> listEnemies = new List<WaveEnemy>();
    public int indexWave;
    public static GameplayManager Instance;
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        BPMStart();
        ChangeBPM(listEnemies[indexBeatsSpawn].bpmDifficulty);
        RefreshUI();
        indexWave = 0;
        //NextEnemy();
        SoundManager.Instance.PlaySoundBGM(SoundManager.Instance.playingBGM);
    }

    // Update is called once per frame
    void Update()
    {
        spdMultiplier = bpm / 60f;
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reset");
            SceneManager.LoadScene(0);
        }
        
        timerEachBeat -= Time.deltaTime;
        if (timerEachBeat >= -0.25 && timerEachBeat <= 0.25f)
        {
            playerCanMove = true;

            if (!juiceIsTriggered)
            {
                ColorPanelRyhthm();
                JuiceObjects();
                juiceIsTriggered = true;
            }
        }
        if (timerEachBeat < 0)
        {
            juiceIsTriggered = false;
            playerCanMove = false;
            timerEachBeat = resetTimerEachBeat;
            Beat();
        }
    }
    

    public void BPMStart()
    {
        //Caclulate the time of each beats depanding of the bpm
        secondBeat = 1 / ((float) listEnemies[indexBeatsSpawn].bpmDifficulty / 60);
        timerEachBeat = secondBeat;
        resetTimerEachBeat = secondBeat;
        timerEachBeat = resetTimerEachBeat;
    }
    public void ChangeBPM(int BPM)
    {
        Debug.Log("BPM : " + BPM);
        SoundManager.Instance.ModifyPitchBGM(BPM/60f);
        bpm = BPM;
        BPMStart();
        RefreshUI();
        
    }

    public void Beat()
    {
        if (indexMetronome <= 3)
        {
            SoundManager.Instance.PlaySoundMetro(SoundManager.Instance.metronomeTic);
        }
        else
        {

            SoundManager.Instance.PlaySoundMetro(SoundManager.Instance.metronomeToc);
            indexMetronome = 0;
        }

        indexMetronome++;
        
        //If there is no enemy
        if (player.target)
        {
            player.target.Action();
            player.ResetBeatPlayer();
        }
        else
        {
            if (indexBeatsSpawn < beatsToSpawn)
            {
                indexBeatsSpawn += 1;
            }
            else
            {
                indexBeatsSpawn = 0;
                NextEnemy();
            }
        }
       
    }

    public void ColorPanelRyhthm()
    {
        Color blanc = Color.white;
        blanc.a = 1;
        
        Color blancMediumAlpha = Color.white;
        blanc.a = 0.5f;
        ryhthmPanel.DOColor(blancMediumAlpha, 0.0f).SetEase(Ease.Linear).OnComplete(() =>
        {
            ryhthmPanel.DOColor(blanc, 0.2f/spdMultiplier).SetEase(Ease.Linear);
        });
    }

    public void JuiceObjects()
    {
        Vector3 scaleObjects = player.transform.localScale;
        Vector3 shrinkScalePlayer = new Vector3(scaleObjects.x,scaleObjects.y - 0.5f,scaleObjects.z);

        if (!player.isDead)
        {
            player.transform.DOScale(shrinkScalePlayer, 0.25f).OnComplete(() =>
                player.transform.DOScale(scaleObjects, 0.25f));
        }


        if (player.target)
        {
            if (!player.target.isDead)
            {
                GameObject enemy = player.target.gameObject;
            
                Vector3 scaleBaseEnemy = enemy.transform.localScale;
                Vector3 scaleEnemyInter = new Vector3(scaleBaseEnemy.x,scaleBaseEnemy.y - 0.5f,scaleBaseEnemy.z);
            
                enemy.transform.DOScale(scaleEnemyInter, 0.25f).OnComplete(() =>
                    enemy.transform.DOScale(scaleBaseEnemy, 0.25f));
            }
        }
    }

    public void NextEnemy()
    {
        WaveEnemy wave = listEnemies[indexWave];
        if (wave.indexEnemy != wave.enemyList.Count)
        {
            //GameObject enemy = SpawnEnemyFromSide(wave.enemyList[wave.indexEnemy],enemySpawn);
            
            GameObject enemy = Instantiate(wave.enemyList[wave.indexEnemy], enemySpot.position,quaternion.identity);
            
            //+1 to index
            wave.indexEnemy += 1;
            player.NextTarget(enemy.GetComponent<Enemy>());
        }
        else
        {
            //If it's a new wave
            indexWave += 1;
            //We take a new BPM
            ChangeBPM(listEnemies[indexWave].bpmDifficulty);
            //Spawn the ennemy
            NextEnemy();
        }
    }

    public void RefreshUI()
    {
        labelLife.text = "Vies : " + lifePlayer;
        WaveEnemy wave = listEnemies[indexWave];
        labelWave.text = "Vague : " + wave.waveNumber + " BPM : " + bpm;
    }

    public GameObject GetActualEnemy()
    {
        WaveEnemy wave = listEnemies[indexWave];
        return wave.enemyList[wave.indexEnemy-1];
    }

    private GameObject SpawnEnemyFromSide(GameObject enemyPrefab, Transform spawnPos)
    {
        GameObject enemy = Instantiate(enemyPrefab, spawnPos.position,quaternion.identity);
        
        Enemy enemyScript = enemy.GetComponent<Enemy>();
        
        enemyScript.enabled = false;
        enemy.transform.DOMove(enemySpot.position, timerEachBeat).SetEase(Ease.InSine);

        return enemy;
    }
}

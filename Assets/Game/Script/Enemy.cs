﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public enum Move
{
    Parrying,
    Attack,
    RdyAttack,
    Vulnerable
}
public class Enemy : MonoBehaviour
{
    private GameplayManager _gameplayManager;
    private Player _player;
    private SpriteRenderer _sprite;
    
    [Header("Life")]
    public int lifeEnemy;
    [SerializeField] private Slider slider;
    public bool isDead;
    
    public bool isParrying;
    private bool isAttaking;

    [Header("Move Set")] 
    public int indexBeat;
    //Depending the number of beats, the enemy will do something
    public List<Move> moveset = new List<Move>();
    public Move nextMove;

    [Header("Knockback")] 
    public Sequence knockbackSequence;

    [Header("Animations")] 
    private Animator _animator;

    private int idPrepareAtkAnim;
    private int idIsVulnerable;
    private int idIsParrying;
    private int idAtk;



    // Start is called before the first frame update
    void Start()
    {
        _gameplayManager = FindObjectOfType<GameplayManager>();
        _player = _gameplayManager.player;
        _sprite = GetComponent<SpriteRenderer>();
        
        slider = GetComponentInChildren<Slider>();
        slider.maxValue = lifeEnemy;
        slider.value = lifeEnemy;
        

        knockbackSequence = DOTween.Sequence();
        
        //Animator Setup
        _animator = GetComponent<Animator>();
        NextAction();
        idIsParrying = Animator.StringToHash("IsParrying");
        idIsVulnerable = Animator.StringToHash("IsVulnerable");
        idPrepareAtkAnim = Animator.StringToHash("Prepare_Atk");
        idAtk = Animator.StringToHash("Atk");
        
        nextMove = moveset[0];
        ChangeStateAnim(nextMove);
        
        _animator.SetFloat("SpeedMultiplier",1*GameplayManager.Instance.spdMultiplier);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int damage)
    {
        if (lifeEnemy > 0)
        {
            if (!_player.willBeCountered)
            {
                lifeEnemy -= damage;
                if (lifeEnemy <= 0)
                {
                    SoundManager.Instance.PlaySoundOther(OtherSounds.EnemyDefeated);
                    _animator.SetTrigger("Death");
                    //Death();
                }
                else
                {
                    slider.value = lifeEnemy;

                    if (isAttaking)
                    {
                        _animator.SetTrigger("Hurt");
                        Knockback();
                    }
                }
            }
            else
            {
                Debug.Log("CounterStrike !");
                //CounterStrike
                ResetAllAnimBool();
                _animator.SetTrigger("Atk");
                //_player.TakeDamage();
            }
        }
    }

    public void Knockback()
    {
        Debug.Log("knockback");
        Vector3 pos = transform.position;
        //transform.DOMoveX(pos.x + 1f, 0f);
        knockbackSequence.Append(transform.DOMoveX(pos.x + 2f, 0.1f).SetEase(Ease.Linear));
        knockbackSequence.PrependInterval(0.5f);
        knockbackSequence.Append(transform.DOMoveX(pos.x, 0.1f).SetEase(Ease.Linear));
    }

  
    public void AttackPlayer()
    {
        if (_player && !isDead)
        {
            if (!_player.isParrying)
            {
                _player.TakeDamage();
            }
        }
    }

    public void NextAction()
    {
        nextMove = moveset[indexBeat];
        ChangeStateAnim(nextMove);
        indexBeat += 1;
        
        switch (nextMove)
        {
            case Move.Attack :
                isAttaking = true;
                _animator.SetTrigger(idAtk);
                transform.DOMove(_gameplayManager.playerSpawn.position, _gameplayManager.timerEachBeat).SetEase(Ease.Linear)
                    .OnComplete(() => transform.DOMove(_gameplayManager.enemySpawn.position, 0f));
                break;
            case Move.Parrying :
                break;
            case Move.RdyAttack :
                break;
            case Move.Vulnerable :
                break;
        }
        
        if (indexBeat >= moveset.Count)
        {
            indexBeat = 0;
        }
    }

    public void Action()
    {
        switch (nextMove)
        {
            case Move.Attack :
                if (!_player.isAttacking)
                {
                    AttackPlayer();
                }
                else
                {
                    //Permet de tuer l'ennemi avant qu'il nous touche lorsque les 2 personnages attaquent
                    if (lifeEnemy - 1 > 0)
                    {
                        AttackPlayer();
                    }
                }
                isAttaking = false;
                break;
            case Move.Parrying :
                isParrying = false;
                break;
        }
        NextAction();
    }

    public void ChangeStateAnim(Move move)
    {
        if (_animator)
        {
            ResetAllAnimBool();
            switch (move)
            {
                case Move.Attack :
                    _animator.SetBool(idPrepareAtkAnim, true);
                    break;
            
                case Move.Parrying :
                    _animator.SetBool(idIsParrying, true);
                    break;
            
                case Move.Vulnerable :
                    _animator.SetBool(idIsVulnerable, true);
                    break;
            }
        }
    }

    
    public void ResetAllAnimBool()
    {
        _animator.SetBool(idIsParrying,false);
        _animator.SetBool(idIsVulnerable,false);
        _animator.SetBool(idPrepareAtkAnim,false);
    }

    #region FunctionsCalledViaAnim
    
    public void Death()
    {
        isDead = true;
        _player.target = null;
        Destroy(gameObject);
    }

    public void TriggerAnim(string nameTrigger)
    {
        if (_animator)
        {
            _animator.SetTrigger(nameTrigger);
        }
    }

    public void IsDefending()
    {
        isParrying = true;
    }
    

    #endregion    
    
}

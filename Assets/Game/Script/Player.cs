﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    private GameplayManager _gameplayManager;
    public bool hasDoneAnAction;
    public bool isParrying;

    public Enemy target;

    private Sequence knockbackSequence;
    //private Sequence attackSequence;

    public bool isDead;
    public bool willBeCountered;
    public bool isAttacking;

    [Header("Animator")] 
    private Animator _animator;
    

    // Start is called before the first frame update
    void Start()
    {
        _gameplayManager = GameplayManager.Instance;
        knockbackSequence = DOTween.Sequence();

        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        //Spd Anim multiplier
        _animator.SetFloat("SpeedMultiplier",1*_gameplayManager.spdMultiplier);

        if (!isDead)
        {
            if (!hasDoneAnAction)
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    if (_gameplayManager.playerCanMove)
                    {
                        if (target)
                        {
                            AttackAnim();
                        }
                    }
                    else
                    {
                        WrongAction();
                    }
                    hasDoneAnAction = true;
                }
            
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    if (_gameplayManager.playerCanMove)
                    {
                        Debug.Log("Parry!");
                        _animator.SetTrigger("Guard");
                        isParrying = true;
                    }
                    else
                    {
                        WrongAction();
                    }
                    hasDoneAnAction = true;
                }
            }
        }
    }

    public void ResetBeatPlayer()
    {
        hasDoneAnAction = false;
        isParrying = false;

    }

    public void WrongAction()
    {
        _gameplayManager.gamePanel.transform.DOShakePosition(0.2f,20f,30);
    }

    public void NextTarget(Enemy enemy)
    {
        target = enemy;
    }

    public void TakeDamage()
    {
        if (_gameplayManager.lifePlayer > 0)
        {
            //Make the player move
            if (!isAttacking)
            {
                Vector3 pos = transform.position;
                knockbackSequence.Append(transform.DOMoveX(pos.x - 5f, 0.2f / _gameplayManager.spdMultiplier).SetEase(Ease.Linear))
                    .PrependInterval(0.5f)
                    .Append(transform.DOMoveX(pos.x, 0.2f / _gameplayManager.spdMultiplier).SetEase(Ease.Linear));
            }

            //Damage calculation
            _gameplayManager.lifePlayer -= 1;
            _gameplayManager.RefreshUI();
            
            //Hurt Anim
            _animator.SetTrigger("Hurt");
            
            //Check Death
            if (_gameplayManager.lifePlayer <= 0)
            {
                _animator.SetTrigger("Death");
                isDead = true;
                Debug.Log("GameOver");
            }
        }
    }

    public void AttackAnim()
    {
        
        isAttacking = true;
        if (target.isParrying)
        {
            willBeCountered = true;
        }
        _animator.SetTrigger("Atk");
        Vector3 pos = new Vector3(transform.position.x,transform.position.y,transform.position.z);
        Debug.Log("Pos 1 " + pos);
        
        Sequence attackSequence = DOTween.Sequence();
        
        attackSequence.Append(transform.DOMove(target.transform.position,0.2f / GameplayManager.Instance.spdMultiplier).SetEase(Ease.Linear))
            .Append(transform.DOMove(pos, 0.1f / GameplayManager.Instance.spdMultiplier).OnComplete(()=>Debug.Log("Pos 2" + pos)).OnComplete(()=> isAttacking = false));
    }

    public void AttackTarget()
    {
        if (!willBeCountered)
        {
            Debug.Log("WillNotBeCountered");
            target.TakeDamage(1);
        }
        else
        {
            Debug.Log("WillBeCountered");
            TakeDamage();
            willBeCountered = false;
        }
    }

    public void DeathPlayer()
    {
        SceneManager.LoadScene(0);
    }
}
